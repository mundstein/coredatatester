<? 


$max = $_GET["n"];
if ($max == 0) {
	$max = 10;
}

$firstNames = array ("John", "Mark", "Joseph", "Ken", "Martin", "Thomas", "Kevin", 
	"Tim", "Tom", "Marcus", "Michael", "Mike", "Ted", "Brian");
$firstNamesCount = count($firstNames)-1;
$lastNames = array("Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis", "Garia", "Rodriguez", "Wilson", "Anderson", "Taylor", "Thomas", "Moore", "Martin", "Jackson", "Thompson", "White", "Lee", "Harris", "Clark", "Lewis", "Robinson", "Walker", "Perez", "Hall", "Young", "Allen", "Sanchez", "Wright", "King", "Scott", "Green", "Baker", "Adams", "Nelson", "Hill", "Campbell", "Mitchell", "Roberts", "Carter", "Phillips", "Evans", "Turner", "Torres", "Parker", "Collins", "Edwards", "Stewart", "Flores", "Morris", "Murphy", "Rivera", "Cook", "Rogers", "Morgan", "Peterson", "Cooper", "Reed", "Bailey", "Bell", "Gomez", "Kelly", "Howard", "Ward", "Cox", "Diaz", "Richardson", "Wood", "Wason", "Brooks", "Bennet", "Gray", "James", "Reyes", "Cruz", "Hughes", "Price", "Myers", "Long", "Forster", "Sanders", "Ross");
$lastNamesCount = count($lastNames)-1;
echo "[ ";
for ($i=0; $i<$max; $i++) {
	$hcp = array (
	   'firstName' => $firstNames[rand(0, $firstNamesCount)],
	   'lastName' => $lastNames[rand(0, $lastNamesCount)],
	   'info' => str_repeat("blabla", rand(0,5)),
	   'isFavorite' => rand(0,10)==0,
	   'male' => "M",
	   'messageReceptivitySegment' => "2",
	   'middleName' => "D.",
	   'notes' => '',
	   'salesPotentialSegment' => "A",
	   'salutation' => "Mr",
	   'suffix' => "MD",
	   'title' => "Dr.",
	   'uid' => rand(0, 56789000),
	   'location_id' => rand(0,298374839),
	   'birthday' => date('Ymd')
	   );
	   echo json_encode($hcp);
	   if ($i != $max-1) { echo ", "; }
	   //flush;
}

echo  "]";

?>

