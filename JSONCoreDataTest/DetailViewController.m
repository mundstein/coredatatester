//
//  DetailViewController.m
//  JSONCoreDataTest
//
//  Created by Sascha Mundstein on 17.01.13.
//  Copyright (c) 2013 Sascha Mundstein. All rights reserved.
//

#import "DetailViewController.h"
#import "MasterViewController.h"
#import "AppDelegate.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

enum Textfields {RecordsTextField = 10, BatchSizeTextField};

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
	
	if (newDetailItem && [newDetailItem class] == [NSString class]) {
		self.detailDescriptionLabel.text = (NSString*)newDetailItem;
		return;
	}
	
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

	if (self.detailItem) {
	    //self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.numRecordsTextField.text = [NSString stringWithFormat:@"%d",[[NSUserDefaults standardUserDefaults] integerForKey:kPrefNumberOfRecordsKey]];
	self.batchSizeField.text = [NSString stringWithFormat:@"%d",[[NSUserDefaults standardUserDefaults] integerForKey:kPrefSaveBatchSizeKey]];
	[self.eraseButton setType:BButtonTypeDanger];
	AppDelegate* del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	if ([del respondsToSelector:@selector(eraseAll)]) {
		[self.eraseButton addTarget:del action:@selector(eraseAll) forControlEvents:UIControlEventTouchUpInside];
	}
	
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:kBackgroundPatternImageName]];
	self.jsonButton.titleLabel.font = [UIFont fontWithName:self.jsonButton.titleLabel.font.fontName size:25.0];

	[self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (void)viewDidUnload {
    [self setNumRecordsTextField:nil];
	[self setProgressView:nil];
	[self setBatchSizeField:nil];
	[self setEraseButton:nil];
	[self setTotalRecordsLabel:nil];
    [super viewDidUnload];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
	if (textField.tag == RecordsTextField) {
		[[NSUserDefaults standardUserDefaults] setInteger:textField.text.integerValue forKey:kPrefNumberOfRecordsKey];
	}
	else if (textField.tag == BatchSizeTextField) {
		[[NSUserDefaults standardUserDefaults] setInteger:textField.text.integerValue forKey:kPrefSaveBatchSizeKey];
	}
}

-(void)buttonPressed {
	[self.numRecordsTextField resignFirstResponder];
	self.progressView.progress = 0;
}

@end
