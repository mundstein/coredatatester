//
//  DetailViewController.h
//  JSONCoreDataTest
//
//  Created by Sascha Mundstein on 17.01.13.
//  Copyright (c) 2013 Sascha Mundstein. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BButton.h"

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) IBOutlet BButton *jsonButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITextField *numRecordsTextField;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UITextField *batchSizeField;
@property (strong, nonatomic) IBOutlet BButton *eraseButton;
@property (strong, nonatomic) IBOutlet UILabel *totalRecordsLabel;

-(IBAction)buttonPressed;

@end
