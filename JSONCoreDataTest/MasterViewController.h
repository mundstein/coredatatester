//
//  MasterViewController.h
//  JSONCoreDataTest
//
//  Created by Sascha Mundstein on 17.01.13.
//  Copyright (c) 2013 Sascha Mundstein. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate, NSURLConnectionDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(IBAction)downloadJSON;

@end
