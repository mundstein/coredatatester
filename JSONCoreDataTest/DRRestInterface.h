//
//  DRLoginService.h
//  DefectRadar
//
//  Created by Clemens Hammerl on 16.09.12.
//  Copyright (c) 2012 DefectRadar OG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "DRSingletonARC.h"

// Entities
#import "Issue.h"
#import "Issue+Extensions.h"

@protocol DRRestDelegate <NSObject>

@optional
-(void)loginSucceeded;
-(void)loginFailedWithError:(NSError *)loginError;

-(void)updatesAvailable;
-(void)updateCheckFailed;

@end

@interface DRRestInterface : NSObject
{
	
    __weak id<DRRestDelegate> delegate;
    
    DRLoggedInUser *loggedInUser;
    NSString *sessionKey;
    ASINetworkQueue *networkQueue;
    
    ASIHTTPRequest *loginWithUsernameRequest;
    ASIHTTPRequest *validateSessionKeyRequest;
    
}

@property (nonatomic, weak) id<DRRestDelegate> delegate;

@property (nonatomic, strong) DRLoggedInUser *loggedInUser;
@property (nonatomic, strong) ASINetworkQueue *networkQueue;
@property (nonatomic, strong) NSString *sessionKey;

+(DRRestInterface *)sharedDRRestInterface;

/*
 Tells the receive wether there is a session key
 stored or not. This method does not tell if the
 stored session key is valid or not
 */
-(BOOL)isSessionKeyAvailable;

/*
 Trys to login with the session key that is available
 */
-(ASIHTTPRequest *)loginWithSessionKey;

/*
 Checks if there are updates for the logged in
 User on the server
 */
-(ASIHTTPRequest *)loginWithUsername:(NSString *)username password:(NSString *)password;

/*
 Tries to do the defectRadar login with the given
 username and passwort
 */
-(ASIHTTPRequest *)checkForUpdates;

/*
 Post a defect to the server - prototyping TODO: remove this mehtod on production
 */
-(ASIHTTPRequest *)postDummyDefect:(Issue *)defectToPost;

/*
 Log out the current user. sets the session key and the loggedin user object
 to nil
 */
-(void)logout;

-(ASIHTTPRequest *)testAPI;

@end