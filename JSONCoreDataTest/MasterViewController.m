//
//  MasterViewController.m
//  JSONCoreDataTest
//
//  Created by Sascha Mundstein on 17.01.13.
//  Copyright (c) 2013 Sascha Mundstein. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"


@interface MasterViewController ()
{
	NSMutableData *_data;
	uint downloadSize;
	uint limit;
	NSDate *downloadStart;
	NSString *dataFileURL;
	
}

@property (nonatomic, strong) NSFileHandle *fileHandle;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@implementation MasterViewController

- (void)awakeFromNib
{
	self.clearsSelectionOnViewWillAppear = NO;
	self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	self.navigationItem.leftBarButtonItem = self.editButtonItem;

	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
	self.navigationItem.rightBarButtonItem = addButton;
	self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
	
	[self.detailViewController view];
	
	[self.detailViewController.jsonButton addTarget:self action:@selector(downloadJSON) forControlEvents:UIControlEventTouchUpInside];

	[self updateCount];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.tableView reloadData];
}

-(void)updateCount {
	NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Contact"];
	NSUInteger count = [self.managedObjectContext countForFetchRequest:fetch error:nil];
	[self.detailViewController.totalRecordsLabel setText:[NSString stringWithFormat:@"Total records: %d", count]];
	
	NSString *sqliteStore = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:kPersistentStoreFileName];
	NSDictionary *attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:sqliteStore error:nil];
	unsigned long long size = [attribs fileSize];
	NSLog(@"SQLite store size: %.2f MB.", size/1024.0/1024.0);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    [newManagedObject setValue:@"Abramovic" forKey:@"lastName"];
    [newManagedObject setValue:@"Aaron" forKey:@"firstName"];
    [newManagedObject setValue:@NO forKey:@"isFavorite"];
    [newManagedObject setValue:@(arc4random() % 1000000) forKey:@"uid"];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
         // Replace this implementation with code to handle the error appropriately.
         // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
	
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    self.detailViewController.detailItem = object;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Contact" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    //[fetchRequest setFetchBatchSize:1000];
    
    // Edit the sort key as appropriate.
    NSArray *sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES],
								 [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES]];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	
	NSLog(@"%s", __PRETTY_FUNCTION__);

    [self.tableView endUpdates];
	[self.tableView beginUpdates];
	for (NSIndexPath *ip in [self.tableView indexPathsForVisibleRows]) {
		[self configureCell:[self.tableView cellForRowAtIndexPath:ip] atIndexPath:ip];
	}
	[self.tableView endUpdates];
	[self updateCount];
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
	NSString *fullName = [NSString stringWithFormat:@"%@, %@",
						  [[object valueForKey:@"lastName"] description],
						  [[object valueForKey:@"firstName"] description]];
    cell.textLabel.text = [NSString stringWithFormat:@"%d. %@", indexPath.row+1,fullName];
}

-(void)downloadJSON {
	
	UIActivityIndicatorView *activity = self.detailViewController.activityIndicator;
	[activity startAnimating];
	[self.detailViewController.detailDescriptionLabel setText:@"Starting download..."];
	self.detailViewController.jsonButton.enabled = NO;
	self.detailViewController.batchSizeField.enabled = NO;
	self.detailViewController.numRecordsTextField.enabled = NO;
	limit = self.detailViewController.numRecordsTextField.text.intValue;
	if (limit == 0) {
		limit = 10000;
		[self.detailViewController.numRecordsTextField setText:[NSString stringWithFormat:@"%d",limit]];
	}
	
	NSString *urlString = [NSString stringWithFormat:@"http://mundstein.at/VEC/testjson.php?n=%d", limit];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	downloadStart = [NSDate date];
	[connection start];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"Connection failed? %@", error);
	_data = nil;
	self.detailViewController.detailDescriptionLabel.text = [NSString stringWithFormat:@"An error occurred: %@", [error localizedDescription]];
	self.detailViewController.jsonButton.enabled = YES;
	self.detailViewController.batchSizeField.enabled = YES;
	self.detailViewController.numRecordsTextField.enabled = YES;
	[self.detailViewController.activityIndicator stopAnimating];
	[self updateCount];
	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	//_data = [[NSMutableData alloc] init];
	dataFileURL = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"data"];
	[[NSFileManager defaultManager] createFileAtPath:dataFileURL contents:nil attributes:nil];
	_fileHandle = [NSFileHandle fileHandleForWritingAtPath:dataFileURL];
	[self.detailViewController.detailDescriptionLabel setText:@"Receiving data from server..."];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	//)[_data appendData:data];
	
	[_fileHandle seekToEndOfFile];
	[_fileHandle writeData:data];
	
	printf("%s", @".".UTF8String);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	[_fileHandle closeFile];
	_fileHandle = nil;

	NSTimeInterval downloadTime = [[NSDate date] timeIntervalSinceDate:downloadStart];
	printf("Done (%.2f seconds).\n", downloadTime);
	
	NSDictionary *attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:dataFileURL error:nil];
	unsigned long long size = [attribs fileSize];
	
	[self.detailViewController.detailDescriptionLabel
	 setText:[NSString stringWithFormat:@"Downlaoded %.2f MB of data in %.2f seconds. Saving...",
			  size/1024.0/1024.0, downloadTime]];
	NSLog(@"Downlaoded %.2f MB of data in %.2f seconds.", size/1024.0/1024.0, downloadTime);
	
	// crashes
	//[_data writeToFile:dataFileURL atomically:YES];
	
	//_data = nil;
	
	connection = nil;
	NSArray *jsonArray;
	@autoreleasepool {
		NSError *error;
		jsonArray = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:dataFileURL] options:NSJSONReadingAllowFragments error:&error];
		if (error) NSLog(@"%@", error);
	}
	
	NSLog(@"Successfully parsed JSON.");
	
	__block uint counter = 0;
	
	NSManagedObjectContext *childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	childContext.persistentStoreCoordinator = self.managedObjectContext.persistentStoreCoordinator;
	childContext.undoManager = nil;
	//childContext.parentContext = self.managedObjectContext;
	
	self.fetchedResultsController.delegate = nil;
	self.fetchedResultsController =  nil;
	NSLog(@"Removed FRC delegate.");

	
	[childContext performBlock:^{
		
		NSDate *start = [NSDate date];
		NSDate *part = start;
		
		
//		int saveBatch = self.detailViewController.batchSizeField.text.intValue;
//		if (saveBatch == 0) saveBatch = 500;
//		[[NSUserDefaults standardUserDefaults] setInteger:saveBatch forKey:kPrefSaveBatchSizeKey];
		int saveBatch = [[NSUserDefaults standardUserDefaults] integerForKey:kPrefSaveBatchSizeKey];
		
		@autoreleasepool {
			
			NSManagedObject *hcp;
			
			for (NSDictionary *dict in jsonArray) {
				@autoreleasepool {
					counter++;
					hcp = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:childContext];
					// populate
					[hcp setValue:dict[@"firstName"] forKey:@"firstName"];
					[hcp setValue:dict[@"lastName"] forKey:@"lastName"];
					[hcp setValue:dict[@"isFavorite"] forKey:@"isFavorite"];
					
					if ((counter % saveBatch) == 0) {
						@autoreleasepool {
							NSDate *now = [NSDate date];
							NSError *error;
							[childContext save:&error];
							if (error) NSLog(@"%@", error);
							//[self.managedObjectContext save:&error];
							dispatch_sync(dispatch_get_main_queue(), ^{
								[self.detailViewController.detailDescriptionLabel setText:
								 [NSString stringWithFormat:@"Saving record %d.", counter]];
								[self.detailViewController.progressView setProgress:counter*1.0/(limit*1.0) animated:YES];
							});
							NSLog(@"Saved %d records in %.2f seconds. (%d total in %.2f seconds.)",
								  saveBatch, [now timeIntervalSinceDate:part], counter, [now timeIntervalSinceDate:start]);
							part = now;
							if (error) NSLog(@"%@", error);
						}
					}
				}
			}
			[childContext save:nil];
			//[self.managedObjectContext save:nil];
			NSLog(@"Final child save.");
		}
		
		
		

		dispatch_sync(dispatch_get_main_queue(), ^{
//			NSLog(@"Now on main queue trying to save.");
//			@autoreleasepool {
//				[self.managedObjectContext save:nil];
//			}
//			NSLog(@"Final save.");

			self.detailViewController.detailDescriptionLabel.text =
			[NSString stringWithFormat:@"Imported and saved all records (%d) successfully in %.2f seconds.", counter, [[NSDate date] timeIntervalSinceDate:start]];
			self.detailViewController.jsonButton.enabled = YES;
			self.detailViewController.batchSizeField.enabled = YES;
			self.detailViewController.numRecordsTextField.enabled = YES;
			[self.detailViewController.activityIndicator stopAnimating];
			[self updateCount];
			
		});
		
//		dispatch_sync(dispatch_get_main_queue(), ^{
//			[self.tableView reloadData];
//			NSLog(@"Set FRC delegate.");
//		});
		
	}];
	
	//childContext = nil;

	
}

- (NSString *)applicationDocumentsDirectory
{
    return [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path];
}



@end
