//
//  DRLoginService.m
//  DefectRadar
//
//  Created by Clemens Hammerl on 16.09.12.
//  Copyright (c) 2012 DefectRadar OG. All rights reserved.
//

#import "DRRestInterface.h"

#import "JSONKit.h"

#define MAX_CONCURRENT_OPERATION_COUNT 6

@interface DRRestInterface (private)


//////////////////// Login ////////////////////
-(void)loginRequestFinished:(ASIHTTPRequest *)request;
-(void)loginRequestFailed:(ASIHTTPRequest *)request;


//////////////////// API Test ////////////////////
-(void)testapiFinished:(ASIHTTPRequest *)request;
-(void)testapiFailed:(ASIHTTPRequest *)request;

///////////////// DUMMY POST //////////////////
-(void)dummyPostFinished:(ASIHTTPRequest *)request;
-(void)dummyPostFailed:(ASIHTTPRequest *)request;

//////////////////// Update check ////////////////////
-(void)updateRequestFinished:(ASIHTTPRequest *)request;
-(void)updatesRequestFailed:(ASIHTTPRequest *)request;

//////////////////// Request Logic ////////////////////
-(void)startRequest:(ASIHTTPRequest *)request;
-(void)requestStarted:(ASIHTTPRequest *)request;
-(void)requestDidReceiveResponseHeader:(ASIHTTPRequest *)request;
-(void)requestFailed:(ASIHTTPRequest *)request;


//////////////////// Helpers ////////////////////
-(NSString *)defectRadarBaseURLString;
-(void)invalidateSessionKey;
-(NSError *)createErrorWithCode:(NSInteger)errorCode errorTitle:(NSString *)title errorMessage:(NSString *)message;

@end

@implementation DRRestInterface

@synthesize delegate;

@synthesize sessionKey;
@synthesize networkQueue;
@synthesize loggedInUser;


SINGLETON_GCD(DRRestInterface);

-(id)init
{
    self = [super init];
    
    if (self) {
        self.sessionKey = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_SESSION_KEY];
        
        
        self.networkQueue = [ASINetworkQueue queue];
        [self.networkQueue setShouldCancelAllRequestsOnFailure:NO];
        self.networkQueue.maxConcurrentOperationCount = MAX_CONCURRENT_OPERATION_COUNT;
        self.networkQueue.delegate = self;
        
        [ASIHTTPRequest setDefaultUserAgentString:DR_USER_AGENT];
        
        
    }
    
    return self;
}

/////////////////////// Public methods /////////////////////////
#pragma mark - public methods

-(BOOL)isSessionKeyAvailable
{
    if (self.sessionKey != nil) {
        return YES;
    }else {
        return NO;
    }
}

-(ASIHTTPRequest *)loginWithUsername:(NSString *)username password:(NSString *)password
{
    [self invalidateSessionKey];
    self.loggedInUser = nil;
    
    NSString *baseURLString = [self defectRadarBaseURLString];
    NSString *apiCallString = [NSString stringWithFormat:@"%@?username=%@&password=%@",LOGIN_URL,username,password];
    NSString *urlString = [baseURLString stringByAppendingString:apiCallString];
	
    loginWithUsernameRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [loginWithUsernameRequest setDidFinishSelector:@selector(loginRequestFinished:)];
    [loginWithUsernameRequest setDidFailSelector:@selector(loginRequestFailed:)];
    
    [self startRequest:loginWithUsernameRequest];
    
    return loginWithUsernameRequest;
    
}

-(ASIHTTPRequest *)loginWithSessionKey
{
    NSString *baseURLString = [self defectRadarBaseURLString];
    NSString *apiCallString = [NSString stringWithFormat:@"%@",LOGIN_WITH_SESSION_KEY_URL];
    NSString *urlString = [baseURLString stringByAppendingString:apiCallString];
    
    validateSessionKeyRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [validateSessionKeyRequest setRequestMethod:@"POST"];
    [validateSessionKeyRequest setDidFinishSelector:@selector(loginRequestFinished:)];
    [validateSessionKeyRequest setDidFailSelector:@selector(loginRequestFailed:)];
    
    [self startRequest:validateSessionKeyRequest];
    
    return validateSessionKeyRequest;
}

/*
 Tries to do the defectRadar login with the given
 username and passwort
 */
-(ASIHTTPRequest *)checkForUpdates
{
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(updateRequestFinished:) userInfo:nil repeats:NO];
	
    return nil;
}

-(void)logout
{
    [self invalidateSessionKey];
    self.loggedInUser = nil;
    
    [[NSNotificationCenter defaultCenter]
	 postNotificationName:NOTIFICATION_LOGGED_OUT
	 object:nil];
}

-(ASIHTTPRequest *)testAPI
{
    
    
    NSString *baseURLString = [self defectRadarBaseURLString];
    NSString *apiCallString = [NSString stringWithFormat:@"%@",TEST_API_URL];
    NSString *urlString = [baseURLString stringByAppendingString:apiCallString];
    
    ASIHTTPRequest *dummyPostRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [dummyPostRequest setRequestMethod:@"POST"];
    [dummyPostRequest setDidFinishSelector:@selector(testapiFinished:)];
    [dummyPostRequest setDidFailSelector:@selector(testapiFailed:)];
    
    [self startRequest:dummyPostRequest];
    
    return dummyPostRequest;
}

/*
 Post a defect to the server - prototyping TODO: remove this mehtod on production
 */
-(ASIHTTPRequest *)postDummyDefect:(Issue *)defectToPost
{
    self.sessionKey = @"3ac7f7f5be74c0fdee9456d4472520b0d74657a5";
    
	
    NSString *urlString = @"http://defectradar.const.at/api/1054957220/projects/1054957222/issues";
	
    
    
    DDLogInfo(@"Created post url %@",urlString);
    
    ASIHTTPRequest *testRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    DDLogInfo(@"Position: %@ - %@",[defectToPost.planPositionX stringValue],[defectToPost.planPositionY stringValue]);
    
    [testRequest appendPostData:[[NSString stringWithFormat:@"{\"subject\":\"%@\",\"plan_x\":\"%@\",\"plan_y\":\"%@\"}",defectToPost.title,[defectToPost.planPositionX stringValue],[defectToPost.planPositionY stringValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    [testRequest setRequestMethod:@"POST"];
    [testRequest addRequestHeader:@"Content-type" value:@"application/json"];
    [testRequest addRequestHeader:@"Accept" value:@"application/json"];
    [testRequest setDidFinishSelector:@selector(dummyPostFinished:)];
    [testRequest setDidFailSelector:@selector(dummyPostFailed:)];
    
    DDLogInfo(@"Starting Request");
    
    [self startRequest:testRequest];
    
    //[SVProgressHUD showWithStatus:@"Posting Defect to server..." maskType:SVProgressHUDMaskTypeNone];
    
    return testRequest;
}

//////////////////// Private Method Implementation //////////////////////
#pragma mark - Private Methods



-(void)loginRequestFinished:(ASIHTTPRequest *)request
{
    DDLogVerbose(@"Request finished %@",request.responseString);
    
    NSString *response = [request responseString];
    NSMutableDictionary *responseDict = [response mutableObjectFromJSONString];
    
    self.sessionKey = [responseDict objectForKey:@"apikey"];
    self.loggedInUser = [[DRLoggedInUser alloc] initWithDictionary:[responseDict objectForKey:@"user"]];
    NSString *message = [responseDict objectForKey:@"message"];
	
    DDLogInfo(@"Session Key %@",self.sessionKey);
    DDLogInfo(@"User %@",self.loggedInUser);
    
    if (self.loggedInUser == nil || self.sessionKey == nil) {
        DDLogError(@"Error while logging in: %@",message);
        //[[NSNotificationCenter defaultCenter]
		//postNotificationName:NOTIFICATION_LOGIN_FAILED
		//object:request];
        
        if (delegate && [delegate respondsToSelector:@selector(loginFailedWithError:)]) {
            
            NSString *errorMessage;
            NSString *errorTitle = NSLocalizedString(@"Login Error", @"Login error title");;
            NSInteger errorCode;
            if (request == loginWithUsernameRequest) {
                errorMessage = NSLocalizedString(@"Invalid username or password", @"Invalid username or password error message");
                
                errorCode = ERROR_INVALID_USERNAME_OR_PASSWORD;
            }else if (request == validateSessionKeyRequest) {
                errorMessage = NSLocalizedString(@"Invalid session key", @"Invalid session key error message");
                
                errorCode = ERROR_SESSION_KEY_INVALID;
            }
            
			
            [delegate loginFailedWithError:[self createErrorWithCode:errorCode errorTitle:errorTitle errorMessage:errorMessage]];
        }
        
    }else {
        DDLogInfo(@"Login successful");
        //[[NSNotificationCenter defaultCenter]
        // postNotificationName:NOTIFICATION_LOGIN_SUCCEED
        // object:request];
        
        // Save the received login credentials for future login requests
        
        [[NSUserDefaults standardUserDefaults] setObject:self.sessionKey forKey:KEY_SESSION_KEY];
        [[NSUserDefaults standardUserDefaults] setObject:self.loggedInUser.login forKey:KEY_LOGGED_IN_USERNAME];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (delegate && [delegate respondsToSelector:@selector(loginSucceeded)]) {
            [delegate loginSucceeded];
        }
        
        
    }
	
    
}

-(void)loginRequestFailed:(ASIHTTPRequest *)request
{
    DDLogInfo(@"Request failed %@",request);
    
    if (delegate && [delegate respondsToSelector:@selector(loginFailedWithError:)]) {
        
        NSString *errorMessage = NSLocalizedString(@"Service not available. Try again later", @"Service not available message");
        NSString *errorTitle = NSLocalizedString(@"Connection Error", @"Connection error title");
        
        [delegate loginFailedWithError:[self createErrorWithCode:ERROR_NO_SERVER_CONNECTION errorTitle:errorTitle errorMessage:errorMessage]];
    }
    
    //[[NSNotificationCenter defaultCenter]
    // postNotificationName:NOTIFICATION_LOGIN_FAILED
    // object:request];
}

//////////////////// Update check ////////////////////
-(void)updateRequestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"updates finished");
    
    if (delegate && [delegate respondsToSelector:@selector(updatesAvailable)]) {
        [delegate updatesAvailable];
    }
}

-(void)updatesRequestFailed:(ASIHTTPRequest *)request
{
    
}

//////////////////// API Test ////////////////////
-(void)testapiFinished:(ASIHTTPRequest *)request
{
    DDLogInfo(@"Testrespone: %@ ",[request responseString]);
}

-(void)testapiFailed:(ASIHTTPRequest *)request
{
    
}

///////////////// DUMMY POST //////////////////
-(void)dummyPostFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD showSuccessWithStatus:@"Defect Submitted"];
    DDLogInfo(@"Dummy Post Finished");
}

-(void)dummyPostFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD showErrorWithStatus:@"Defect submissopn failed"];
    DDLogError(@"Dummy Post Failed");
}

//////////////////// Request Logic //////////////////////
#pragma mark - Request Logic

-(void)startRequest:(ASIHTTPRequest *)request
{
    DDLogVerbose(@"Starting Request %@ with session key %@",request,self.sessionKey);
    
    // Setting the session key in the http header
    if (self.sessionKey) {
        //NSMutableDictionary *requestHeader = [[NSMutableDictionary alloc] init];
        [request addRequestHeader:KEY_REDMINE_HTTP_HEADER value:self.sessionKey];
        //[requestHeader setObject:self.sessionKey forKey:KEY_REDMINE_HTTP_HEADER];
        //[request setRequestHeaders:requestHeader];
        
    }
    
    //[request addRequestHeader:@"Authorization" value:[NSString stringWithFormat:@"Basic %@",[ASIHTTPRequest base64forData:[[NSString stringWithFormat:@"%@:%@",HTTP_AUTH_USER,HTTP_AUTH_PASS] dataUsingEncoding:NSUTF8StringEncoding]]]];
    
    // Set the http authentication credentials
    
    [request addBasicAuthenticationHeaderWithUsername:HTTP_AUTH_USER
                                          andPassword:HTTP_AUTH_PASS];
    
    //[request setUsername:HTTP_AUTH_USER];
    //[request setPassword:HTTP_AUTH_PASS];
	
	
    request.delegate = self;
    
    [request setDidStartSelector:@selector(requestStarted:)];
    [request setDidReceiveResponseHeadersSelector:@selector(requestDidReceiveResponseHeader:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    DDLogVerbose(@"Request Header: %@",request.requestHeaders);
    
    // adding the request to the queue
    [self.networkQueue addOperation:request];
    
    // telling the queue to proceed
    [self.networkQueue go];
    
}

-(void)requestStarted:(ASIHTTPRequest *)request
{
    DDLogInfo(@"Request started %@",request.url);
    DDLogVerbose(@"Request Header %@",request.requestHeaders);
}

-(void)requestDidReceiveResponseHeader:(ASIHTTPRequest *)request
{
    int httpStatusCode = request.responseStatusCode;
    
    if (request == loginWithUsernameRequest || request == validateSessionKeyRequest) {
        NSLog(@"valid request");
    }else {
        NSLog(@"Other request");
    }
    
    DDLogInfo(@"Resopnse Status Code: %i",httpStatusCode);
    DDLogInfo(@"Request received response Header %@",request.responseHeaders);
	
    if (httpStatusCode != 200) {
        
        if (request == validateSessionKeyRequest || request == loginWithUsernameRequest) {
            
            [self loginRequestFailed:request];
            
        }
        
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    DDLogError(@"Request failed %@ Error: %@",request,request.error);
}

//////////////////// Helper Functions //////////////////////
#pragma mark - Helper Functions

-(NSString *)defectRadarBaseURLString
{
    return [NSString stringWithFormat:@"%@://%@/%@/", DR_PROTOCOL,ROOT_URL,API_URL];
}

-(void)invalidateSessionKey
{
    if (sessionKey) {
        // invalidate session key if exists
        self.sessionKey = nil;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KEY_SESSION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(NSError *)createErrorWithCode:(NSInteger)errorCode errorTitle:(NSString *)title errorMessage:(NSString *)message
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:message forKey:KEY_ERROR_MESSAGE];
    [userInfo setObject:title forKey:KEY_ERROR_TITLE];
    
    return [[NSError alloc] initWithDomain:@"DRRestInterface" code:errorCode userInfo:userInfo];
}

@end